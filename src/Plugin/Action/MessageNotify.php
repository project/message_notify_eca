<?php

namespace Drupal\message_notify_eca\Plugin\Action;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\eca\EcaState;
use Drupal\eca\Token\TokenInterface;
use Drupal\message\Entity\Message;
use Drupal\message_notify\Exception\MessageNotifyException;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\message_notify\MessageNotifier;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Send a message by email action.
 *
 * @Action(
 *   id = "message_notify",
 *   label = @Translation("Send a message by email"),
 *   type = "message",
 *   category = @Translation("Message")
 * )
 */
class MessageNotify extends ConfigurableActionBase {

  /**
   * The message notifier.
   *
   * @var \Drupal\message_notify\MessageNotifier|null
   */
  private ?MessageNotifier $notifier = NULL;

  /**
   * Gets the message notifier.
   *
   * @return \Drupal\message_notify\MessageNotifier
   *   The message notifier.
   */
  protected function notifier(): MessageNotifier {
    if (!isset($this->notifier)) {
      $this->notifier = \Drupal::service('message_notify.sender');
    }
    return $this->notifier;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'from' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['from'] = [
      '#title' => $this->t('From'),
      '#type' => 'textfield',
      '#required' => FALSE,
      '#default_value' => $this->configuration['from'],
      '#description' => $this->t('Provide a from email if it is necessary.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['from'] = $form_state->getValue('from');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function access($message, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if (!$account instanceof AccountInterface) {
      /** @var \Drupal\message\MessageInterface $message */
      $account = $message->getOwner();
    }
    $access = AccessResult::allowedIf($account->isAuthenticated());
    return $return_as_object ? $access : $access->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function execute(Message $message = NULL): void {

    /** @var \Drupal\message\Entity\MessageTemplate $template */
    $template = $message->getTemplate();
    if (!empty($template->getText($message->getOwner()->getPreferredLangcode()))) {
      $message->setLanguage($message->getOwner()->getPreferredLangcode());
    }

    $options = ['language override' => TRUE];
    if (!empty($this->configuration['from'])) {
      $options['from'] = $this->configuration['from'];
    }

    try {
      $this->notifier()->send($message, $options);
    }
    catch (MessageNotifyException $e) {
      $this->messenger()->addError($this->t('Email Send Notification failed.'));
    }
  }

}

